FROM debian:buster

RUN apt update && \
    apt install -y texlive-full && \
    apt install -y make && \
    useradd -ms /bin/bash latex && \
    mkdir /workdir

USER latex
WORKDIR /workdir
